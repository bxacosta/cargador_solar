#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

// LiquidCrystal
const int rs = 11, en = 10, d4 = 6, d5 = 7, d6 = 8, d7 = 9;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int TX = 2;
int RX = 3;

SoftwareSerial wifiSerial(RX, TX); // RX, TX

//Charger Inputs
const int I1 = 18;
const int I2 = 19;

//CHarger Value
int v1;
int v2;

//Array to battery symbol
byte b[8];

String lastState1;
String currentState1;
String lastState2;
String currentState2;

unsigned long previousMillis = 0;
int index = 0;

void setup() {
  wifiSerial.begin(115200);

  for (int i = 0; i < 6; i++) {
    getBatterySymbol(i);
    lcd.createChar(i, b);
  }

  lcd.begin(16, 2);
  lcd.print("Cargador  v:");

  // Print USB1
  lcd.setCursor(0, 1);
  lcd.print("USB1:");

  // Print USB1
  lcd.setCursor(10, 1);
  lcd.print("USB2:");

  v1 = analogRead(I1);
  v2 = analogRead(I2);

  //Init State 1
  if (v1 > 0) {
    lastState1 = "on";
  } else {
    lastState1 = "off";
  }

  //Init State 2
  if (v2 > 0) {
    lastState2 = "on";
  } else {
    lastState2 = "off";
  }
}

void loop() {
  float voltage = calculeInputVoltage();

  // Print input voltage
  lcd.setCursor(12, 0);
  lcd.print(voltage);


  v1 = analogRead(I1);
  v2 = analogRead(I2);


  unsigned long currentMillis = millis();

  if ( currentMillis - previousMillis > 500 ) {
    previousMillis = currentMillis;
    index++;
  }

  // Set the current state
  if (v1 > 0) {
    currentState1 = "on";

    drawBattery(1, index%5);
  } else {
    currentState1 = "off";

    drawBattery(1, 5);
  }
  if (v2 > 0) {
    currentState2 = "on";

    drawBattery(2, index%5);
  } else {
    currentState2 = "off";

    drawBattery(2, 5);
  }

  //Send data if the state changes
  if (!lastState1.equals(currentState1)) {
    wifiSerial.print("1-" + currentState1);
    lastState1 = currentState1;
  }

  if (!lastState2.equals(currentState2)) {
    wifiSerial.print("2-" + currentState2);
    lastState2 = currentState2;
  }

  if(index > 1000) {
    index = 0;
  }
  
  delay(100);
}

float calculeInputVoltage() {
  // Max voltage input read
  float MAX_V = 4.84;
  // Voltage Divider
  float R1 = 2180.0;  // Positive termonal
  float R2 = 980.0;  // Negative terminal

  float voltage = 0.0;

  int value = analogRead(A7);

  voltage = ((R1 + R2) / R2) * (float)value * (MAX_V / 1023.0);

  return voltage;
}


void getBatterySymbol(int n) {
  switch (n) {
    case 0:
      b[0] = B01110;
      b[1] = B11111;
      b[2] = B10001;
      b[3] = B10001;
      b[4] = B10001;
      b[5] = B10001;
      b[6] = B11111;
      break;
    case 1:
      b[0] = B01110;
      b[1] = B11111;
      b[2] = B10001;
      b[3] = B10001;
      b[4] = B10001;
      b[5] = B11111;
      b[6] = B11111;
      break;
    case 2:
      b[0] = B01110;
      b[1] = B11111;
      b[2] = B10001;
      b[3] = B10001;
      b[4] = B11111;
      b[5] = B11111;
      b[6] = B11111;
      break;
    case 3:
      b[0] = B01110;
      b[1] = B11111;
      b[2] = B10001;
      b[3] = B11111;
      b[4] = B11111;
      b[5] = B11111;
      b[6] = B11111;
      break;
    case 4:
      b[0] = B01110;
      b[1] = B11111;
      b[2] = B11111;
      b[3] = B11111;
      b[4] = B11111;
      b[5] = B11111;
      b[6] = B11111;
      break;
    case 5:
      b[0] = B00100;
      b[1] = B00000;
      b[2] = B00000;
      b[3] = B00000;
      b[4] = B00000;
      b[5] = B00000;
      b[6] = B00000;
      break;
  }
}

void drawBattery(int number, int symbolIndex) {
  if (number == 1) {
    lcd.setCursor(5, 1);
  } else if (number == 2) {
    lcd.setCursor(15, 1);
  }

  lcd.write(byte(symbolIndex));
}


