#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti wifiMulti;

const int   infoLed = 2;

const char* ssid     = "NigmA";
const char* password = "nigma1234";

const char* host  = "192.168.137.1";
const int   port  = 8080;
const String resource = "esp/ESPServlet";

void setup() {
  Serial.begin(115200);
  delay(1000);

  //Setup the led
  pinMode(infoLed, OUTPUT);
  digitalWrite(infoLed, HIGH);

  // Setup the wifi networks
  WiFi.mode(WIFI_STA);
  wifiMulti.addAP(ssid, password);

  // Conect to a network
  Serial.println("Connecting Wifi...");
  while (wifiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // WiFi connected
  digitalWrite(infoLed, LOW);

  Serial.println("\nWiFi connected");
  Serial.print("SSID:\t");
  Serial.println(WiFi.SSID());
  Serial.print("IP:\t");
  Serial.println(WiFi.localIP());
}

void loop() {

  if (wifiMulti.run() == WL_CONNECTED) {
    if (Serial.available() > 0) {

      String data = Serial.readString();

        Serial.println(data);
        sendData("dato=" + data);
    }
  } else {
    digitalWrite(infoLed, HIGH);

    // Conect to a network
    Serial.println("\nConnection lost, reconnecting Wifi...");
    while (wifiMulti.run() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
    }

    // WiFi connected
    digitalWrite(infoLed, LOW);

    Serial.println("\nWiFi connected");
    Serial.print("SSID:\t");
    Serial.println(WiFi.SSID());
    Serial.print("IP:\t");
    Serial.println(WiFi.localIP());
  }
}

void sendData(String data) {
  WiFiClient client;

  if (client.connect(host, port)) {
    // POST request
    client.print(String("POST /") + resource + " HTTP/1.1\r\n" +
                 "Host: " + String(host) + ":" + String(port) + "\r\n" +
                 "Content-Length: " + data.length() + "\r\n" +
                 "Content-Type:" + "application/x-www-form-urlencoded\r\n" +
                 "\r\n" +
                 data);

    // Print the response
    if (client.available()) {
      String line = client.readStringUntil('\n');
      Serial.println(line);
    }
  } else {

    // Connection with the host fail
    for (int i = 0; i < 4; i++) {
      digitalWrite(infoLed, HIGH);
      delay(50);
      digitalWrite(infoLed, LOW);
      delay(50);
    }
  }
}
